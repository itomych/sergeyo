package sergeyomelyanovski.com.githubresume.resume

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sergeyomelyanovski.com.githubresume.data.entity.UserRepo
import sergeyomelyanovski.com.githubresume.databinding.ItemRepoBinding


class ReposListAdapter : RecyclerView.Adapter<ReposListAdapter.UserRepoViewHolder>() {
    private val repos = mutableListOf<UserRepo>()
    private var inflater: LayoutInflater? = null

    private fun getLayoutInflater(context: Context) : LayoutInflater {
        inflater?.let {
            return it
        } ?: run {
            val inflater = LayoutInflater.from(context)
            this.inflater = inflater
            return inflater
        }
    }

    fun setItems(repos: List<UserRepo>) {
        this.repos.clear()
        this.repos.addAll(repos)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserRepoViewHolder {
        val binding = ItemRepoBinding.inflate(getLayoutInflater(parent.context), parent, false)
        binding.viewModel = UserRepoViewModel()
        return UserRepoViewHolder(binding)
    }

    override fun getItemCount() = repos.size

    override fun onBindViewHolder(holder: UserRepoViewHolder, position: Int) {
        holder.bind(repos[position])
    }


    class UserRepoViewHolder(private val binding: ItemRepoBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(userRepo: UserRepo) {
            binding.viewModel?.start(userRepo)
            binding.repoNameTextView.setOnClickListener {
                val url = userRepo.clone_url
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                it.context.startActivity(intent)
            }
        }
    }
}