package sergeyomelyanovski.com.githubresume.resume

import android.app.Activity
import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import sergeyomelyanovski.com.githubresume.common.obtainViewModel
import sergeyomelyanovski.com.githubresume.databinding.FragmentUserResumeBinding

const val OBTAIN_VIEWMODEL_EXCEPTION_MESSAGE = "Activity is null when trying to obtain viewModel"

class ResumeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentUserResumeBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        val viewModel = obtainViewModel(ResumeViewModel::class.java)
        binding.viewModel = viewModel
        arguments?.let {
            val nickname = ResumeFragmentArgs.fromBundle(it).nickname
            viewModel.start(nickname)
        }
        observeViewModelEvents(viewModel)
        return binding.root
    }

    private fun observeViewModelEvents(viewModel: ResumeViewModel) {
        viewModel.userNotFoundEvent.observe(this, Observer {
            processError("User not found")
        })
        viewModel.connectionFailedEvent.observe(this, Observer {
            processError("Connection failed!")
        })
        viewModel.networkErrorEvent.observe(this, Observer {
            processError(it)
        })
    }

    private fun processError(message: String) {
        showToast(message)
        findNavController().popBackStack()
    }

    private fun showToast(message: String) {
        activity?.run {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.hideKeyboard()
    }
}

fun Activity.hideKeyboard() {
    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}
