package sergeyomelyanovski.com.githubresume.resume

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import sergeyomelyanovski.com.githubresume.data.entity.UserRepo

class UserRepoViewModel : ViewModel() {

    val name = ObservableField<String>()
    val link = ObservableField<String>()

    fun start(userRepo: UserRepo) {
        name.set(userRepo.name)
        link.set(userRepo.clone_url)
    }
}