package sergeyomelyanovski.com.githubresume.resume

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import sergeyomelyanovski.com.githubresume.common.BaseViewModel
import sergeyomelyanovski.com.githubresume.data.ConnectionError
import sergeyomelyanovski.com.githubresume.data.NetworkError
import sergeyomelyanovski.com.githubresume.data.Repository
import sergeyomelyanovski.com.githubresume.data.ResultObject
import sergeyomelyanovski.com.githubresume.data.entity.User

class ResumeViewModel(application: Application, repository: Repository) :
    BaseViewModel(application, repository) {

    val adapter = ReposListAdapter()
    private val _userNotFoundEvent = MutableLiveData<Unit>()
    val userNotFoundEvent: LiveData<Unit> = _userNotFoundEvent
    private val _connectionFailedEvent = MutableLiveData<Unit>()
    val connectionFailedEvent: LiveData<Unit> = _connectionFailedEvent
    private val _networkErrorEvent = MutableLiveData<String>()
    val networkErrorEvent: LiveData<String> = _networkErrorEvent
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading
    private val _userName = MutableLiveData<String>()
    val userName: LiveData<String> = _userName
    private val _userLink = MutableLiveData<String>()
    val userLink: LiveData<String> = _userLink
    private var userData: LiveData<ResultObject<User>>? = null
    private var userDataObserver = Observer<ResultObject<User>> {
        _isLoading.postValue(false)
        when (it) {
            is ResultObject.SuccessResult -> {
                it.getResult()?.run {
                    _userName.postValue(name ?: login)
                    _userLink.postValue(it.getResult()?.blog)
                    repos?.let { userRepos ->
                        adapter.setItems(userRepos)
                    }
                }
            }
            is ResultObject.ErrorResult -> {
                Log.e("ResumeViewModel", it.t.message, it.t)
                _networkErrorEvent.postValue(it.t.message)
            }

            is NetworkError -> {
                if (it.code == 404) {
                    _userNotFoundEvent.postValue(Unit)
                } else {
                    _networkErrorEvent.postValue(it.t.message)
                }
            }

            is ConnectionError -> {
                _connectionFailedEvent.postValue(Unit)
            }
        }
    }

    fun start(nickname: String) {
        _isLoading.postValue(true)
        userData = repository.getUser(nickname)
        userData?.observeForever(userDataObserver)
    }

    override fun onCleared() {
        super.onCleared()
        userData?.removeObserver(userDataObserver)
    }
}