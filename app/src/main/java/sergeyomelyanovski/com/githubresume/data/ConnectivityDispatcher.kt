package sergeyomelyanovski.com.githubresume.data

import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

class ConnectivityDispatcher private constructor(private val connectivityManager: ConnectivityManager) :
    ConnectivityStatus {

    companion object {

        @Volatile
        private var INSTANCE: ConnectivityDispatcher? = null

        fun getInstance(connectivityManager: ConnectivityManager): ConnectivityDispatcher =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: init(connectivityManager).also {
                    INSTANCE = it
                }
            }

        private fun init(connectivityManager: ConnectivityManager) =
            ConnectivityDispatcher(connectivityManager)
    }

    @Suppress("DEPRECATION")
    override fun hasConnection(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager.run {
                getNetworkCapabilities(activeNetwork)?.run {
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) or hasTransport(
                        NetworkCapabilities.TRANSPORT_CELLULAR
                    )
                } ?: false
            }
        } else connectivityManager.activeNetworkInfo?.isConnected ?: false
    }
}