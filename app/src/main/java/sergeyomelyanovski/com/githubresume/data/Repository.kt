package sergeyomelyanovski.com.githubresume.data

import androidx.lifecycle.LiveData
import sergeyomelyanovski.com.githubresume.data.entity.User

interface Repository {

    fun getUser(nickname: String): LiveData<ResultObject<User>>
}