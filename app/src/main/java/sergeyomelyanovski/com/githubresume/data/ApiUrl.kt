package sergeyomelyanovski.com.githubresume.data

const val BASE_URL = "https://api.github.com/"
const val PATH_USERNAME = "username"
const val QUERY_PAGE = "page"
const val GET_USER = "/users/{$PATH_USERNAME}"
const val GET_REPOS = "/users/{$PATH_USERNAME}/repos"
