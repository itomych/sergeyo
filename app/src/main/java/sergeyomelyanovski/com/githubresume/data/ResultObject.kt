package sergeyomelyanovski.com.githubresume.data

sealed class ResultObject<out T : Any?>(protected val resultData: T?) {

    class SuccessResult<out T : Any?>(result: T) : ResultObject<T>(result) {
        fun getResult() = resultData
    }

    open class ErrorResult(open val t: Throwable) : ResultObject<Nothing>(null) {

        open fun map(): ErrorResult {
            return ErrorResult(t)
        }
    }
}

class ConnectionError : ResultObject.ErrorResult(Throwable("Internet connection failed!"))

class NetworkError(val code: Int, t: Throwable) : ResultObject.ErrorResult(t)