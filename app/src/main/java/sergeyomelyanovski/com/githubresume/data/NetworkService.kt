package sergeyomelyanovski.com.githubresume.data

import sergeyomelyanovski.com.githubresume.data.entity.User

interface NetworkService {

    suspend fun getUser(nickName: String): ResultObject<User>
}