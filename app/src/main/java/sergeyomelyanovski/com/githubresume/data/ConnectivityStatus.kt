package sergeyomelyanovski.com.githubresume.data

interface ConnectivityStatus {

    fun hasConnection(): Boolean
}