package sergeyomelyanovski.com.githubresume.data

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import sergeyomelyanovski.com.githubresume.BuildConfig
import sergeyomelyanovski.com.githubresume.data.entity.User
import sergeyomelyanovski.com.githubresume.data.entity.UserRepo

interface GithubService {

    @GET(GET_USER)
    fun getUser(@Path(PATH_USERNAME) sort: String): Deferred<Response<User>>

    @GET(GET_REPOS)
    fun getRepos(@Path(PATH_USERNAME) sort: String, @Query(QUERY_PAGE) page: Int? = null): Deferred<Response<List<UserRepo>>>

    companion object Factory {

        private lateinit var service: GithubService

        fun create(): GithubService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(createClient())
                .baseUrl(BASE_URL)
                .build()
            service = retrofit.create(GithubService::class.java)
            return service
        }

        private fun createClient(): OkHttpClient {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            var httpClient = OkHttpClient.Builder()
            if (BuildConfig.DEBUG) {
                httpClient = httpClient.addInterceptor(logging)
            }
            return httpClient.build()
        }
    }
}