package sergeyomelyanovski.com.githubresume.data.entity

data class UserRepo(
    val name: String,
    val id: Int,
    val git_url: String,
    val clone_url: String,
    val url: String
)