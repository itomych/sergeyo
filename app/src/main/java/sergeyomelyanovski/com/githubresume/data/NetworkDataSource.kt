package sergeyomelyanovski.com.githubresume.data

import sergeyomelyanovski.com.githubresume.data.entity.User
import sergeyomelyanovski.com.githubresume.data.entity.UserRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NetworkDataSource private constructor(
    private val connectivityStatus: ConnectivityStatus
) : NetworkService {
    private var service = GithubService.create()

    companion object {
        @Volatile
        private var INSTANCE: NetworkDataSource? = null

        fun getInstance(connectivityStatus: ConnectivityStatus): NetworkDataSource =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: init(connectivityStatus).also {
                    INSTANCE = it
                }
            }

        private fun init(connectivityStatus: ConnectivityStatus) =
            NetworkDataSource(connectivityStatus)
    }

    override suspend fun getUser(nickName: String): ResultObject<User> =
        withContext(Dispatchers.IO) {
            if (!connectivityStatus.hasConnection()) {
                ConnectionError()
            }

            try {
                val userResponse = service.getUser(nickName).await()
                if (userResponse.isSuccessful) {
                    val repos = mutableListOf<UserRepo>()
                    val user = userResponse.body()
                    service.getRepos(nickName).await().body()?.let {
                        //load first page
                        repos.addAll(it)
                    }
                    var page = 2
                    while (repos.size < user?.public_repos ?: 0) {
                        service.getRepos(nickName, page).await().body()?.let {
                            repos.addAll(it)
                        }

                        page += 1
                    }
                    user?.let {
                        ResultObject.SuccessResult(User(user, repos))
                    } ?: run {
                        ResultObject.ErrorResult(Throwable("User info was not loaded"))
                    }
                } else {
                    NetworkError(userResponse.code(), Throwable(userResponse.message()))
                }
            } catch (e: Exception) {
                ResultObject.ErrorResult(e)
            }
        }
}