package sergeyomelyanovski.com.githubresume.data.entity

data class User(
    val blog: String?,
    val url: String,
    val name: String?,
    val login: String,
    val id: Int,
    val public_repos: Int,
    val repos_url: String,
    val repos: List<UserRepo>?
) {
    constructor(user: User, repos: List<UserRepo>) : this(
        user.blog,
        user.url,
        user.name,
        user.login,
        user.id,
        user.public_repos,
        user.repos_url,
        repos
    )
}