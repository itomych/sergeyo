package sergeyomelyanovski.com.githubresume.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import sergeyomelyanovski.com.githubresume.data.entity.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class GithubResumeRepository private constructor(
    private val networkService: NetworkService
) : Repository, CoroutineScope {


    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    companion object {
        @Volatile
        private var INSTANCE: GithubResumeRepository? = null

        fun getInstance(networkService: NetworkService): GithubResumeRepository =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: init(networkService).also {
                    INSTANCE = it
                }
            }

        private fun init(networkService: NetworkService) = GithubResumeRepository(networkService)
    }

    override fun getUser(nickname: String): LiveData<ResultObject<User>> {
        val data = MediatorLiveData<ResultObject<User>>()
        launch {
            data.postValue(networkService.getUser(nickname))
        }
        return data
    }
}