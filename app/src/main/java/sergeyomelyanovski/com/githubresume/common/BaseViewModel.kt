package sergeyomelyanovski.com.githubresume.common

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import sergeyomelyanovski.com.githubresume.data.Repository

open class BaseViewModel(application: Application, protected val repository: Repository) :
    AndroidViewModel(application)