package sergeyomelyanovski.com.githubresume.common

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import sergeyomelyanovski.com.githubresume.resume.OBTAIN_VIEWMODEL_EXCEPTION_MESSAGE

fun <T : ViewModel> Fragment.obtainViewModel(clazz: Class<T>): T {
    return activity?.run {
        obtainViewModel(ViewModelFactory.getInstance(application), clazz)
    } ?: throw IllegalStateException(OBTAIN_VIEWMODEL_EXCEPTION_MESSAGE)
}

fun <VM : ViewModel> Fragment.obtainViewModel(
    factory: ViewModelProvider.Factory?,
    cls: Class<VM>
): VM {
    return ViewModelProviders.of(this, factory).get(cls)
}