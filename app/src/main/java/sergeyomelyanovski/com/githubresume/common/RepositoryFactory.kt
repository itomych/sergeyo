package sergeyomelyanovski.com.githubresume.common

import android.content.Context
import android.net.ConnectivityManager
import sergeyomelyanovski.com.githubresume.data.ConnectivityDispatcher
import sergeyomelyanovski.com.githubresume.data.GithubResumeRepository
import sergeyomelyanovski.com.githubresume.data.NetworkDataSource
import sergeyomelyanovski.com.githubresume.data.Repository

class RepositoryFactory {

    companion object {
        fun provideRepository(context: Context): Repository {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val connectivityStatus = ConnectivityDispatcher.getInstance(connectivityManager)
            val networkDataSource = NetworkDataSource.getInstance(connectivityStatus)
            return GithubResumeRepository.getInstance(networkDataSource)
        }
    }
}