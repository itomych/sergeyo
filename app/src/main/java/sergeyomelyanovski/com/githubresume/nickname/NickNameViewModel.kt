package sergeyomelyanovski.com.githubresume.nickname

import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NickNameViewModel : ViewModel() {
    private val _isEnabled = MutableLiveData<Boolean>()
    val enabled: LiveData<Boolean> = _isEnabled

    init {
        _isEnabled.postValue(false)
    }

    fun onNicknameChanged(nickname: String) {
        _isEnabled.postValue(!TextUtils.isEmpty(nickname))
    }
}