package sergeyomelyanovski.com.githubresume.nickname

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import sergeyomelyanovski.com.githubresume.databinding.FragmentNicknameBinding
import sergeyomelyanovski.com.githubresume.common.obtainViewModel

class NickNameFragment : Fragment(), NicknameActionListener {
    private lateinit var binding: FragmentNicknameBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentNicknameBinding.inflate(inflater, container, false)
        binding.apply {
            listener = this@NickNameFragment
            lifecycleOwner = this@NickNameFragment
            viewModel = getNickNameViewModel()
        }
        return binding.root
    }

    override fun onGenerateButtonClick() {
        val directions = NickNameFragmentDirections.actionNicknameFragmentToResumeFragment(
            binding.nicknameEditText.text.toString()
        )
        findNavController().navigate(directions)
    }

    override fun nicknameTextChanged(nickname: String) {
        getNickNameViewModel().onNicknameChanged(nickname)
    }

    private fun getNickNameViewModel() = obtainViewModel(NickNameViewModel::class.java)
}