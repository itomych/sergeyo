package sergeyomelyanovski.com.githubresume.nickname

interface NicknameActionListener {

    fun onGenerateButtonClick()

    fun nicknameTextChanged(nickname: String)
}